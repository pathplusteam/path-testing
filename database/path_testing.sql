-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 07, 2012 at 03:02 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `path_testing`
--

-- --------------------------------------------------------

--
-- Table structure for table `boarddata`
--

CREATE TABLE IF NOT EXISTS `boarddata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `boardid` int(11) NOT NULL,
  `data` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `boardid` (`boardid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `boarddata`
--

INSERT INTO `boarddata` (`id`, `boardid`, `data`) VALUES
(32, 21, '[{"nodes":[{"css":{"left":"1133px","top":"177px","width":"50px","height":"20px","position":"absolute","z-index":"20"},"id":"0_node","realId":0,"outEdgeArray":[],"name":"0_node","description":"This is my first node","script":{"array":[{"nodeId":"0_node","name":"0_node--&gt;0_node","content":"This is my Test Script for connection 1"},{"nodeId":"0_node","name":"0_node--&gt;1_node","content":"This is my Test Script for connection 3"}]},"start":false,"end":false},{"css":{"left":"793px","top":"449px","width":"50px","height":"20px","position":"absolute","z-index":"20"},"id":"1_node","realId":1,"outEdgeArray":[],"name":"1_node","description":"","script":{"array":[]},"start":true,"end":false}]},{"links":[{"source":"0_node","target":"0_node","sourceType":"TopCenter","targetType":"RightMiddle"},{"source":"0_node","target":"1_node","sourceType":"BottomCenter","targetType":"TopCenter"},{"source":"1_node","target":"0_node","sourceType":"LeftMiddle","targetType":"LeftMiddle"}]},{"id":2}]'),
(33, 22, '[{"nodes":[{"css":{"left":"472px","top":"231px","width":"50px","height":"20px","position":"absolute","z-index":"20"},"id":"0_node","realId":0,"outEdgeArray":[],"name":"0_node","description":"","script":{"array":[{"nodeId":"0_node","name":"0_node--&gt;1_node","content":"Hello My name is chris\\nI was wondering what time it was\\n"}]},"start":false,"end":false},{"css":{"left":"1130px","top":"447px","width":"50px","height":"20px","position":"absolute","z-index":"20"},"id":"1_node","realId":1,"outEdgeArray":[],"name":"1_node","description":"","script":{"array":[{"nodeId":"1_node","name":"1_node--&gt;0_node","content":"box 1"},{"nodeId":"1_node","name":"1_node--&gt;0_node","content":"box 2"}]},"start":false,"end":false},{"css":{"left":"407px","top":"734px","width":"50px","height":"20px","position":"absolute","z-index":"20"},"id":"2_node","realId":2,"outEdgeArray":[],"name":"2_node","description":"","script":{"array":[{"nodeId":"2_node","name":"2_node--&gt;1_node","content":""}]},"start":false,"end":false}]},{"links":[{"source":"0_node","target":"1_node","sourceType":"RightMiddle","targetType":"TopCenter"},{"source":"1_node","target":"0_node","sourceType":"LeftMiddle","targetType":"LeftMiddle"},{"source":"1_node","target":"0_node","sourceType":"BottomCenter","targetType":"BottomCenter"},{"source":"2_node","target":"1_node","sourceType":"BottomCenter","targetType":"RightMiddle"}]},{"id":3}]'),
(34, 23, '[{"nodes":[{"css":{"left":"640px","top":"255px","width":"50px","height":"20px","position":"absolute","z-index":"20"},"id":"0_node","realId":0,"outEdgeArray":[],"name":"0_node","description":"","script":{"array":[{"nodeId":"0_node","name":"0_node--&gt;1_node","precondition":"hello this is my precondition","body":"hello this is my body\\n"}]},"start":false,"end":false},{"css":{"left":"468px","top":"179px","width":"50px","height":"20px","position":"absolute","z-index":"20"},"id":"1_node","realId":1,"outEdgeArray":[],"name":"1_node","description":"","script":{},"start":false,"end":false}]},{"links":[{"source":"0_node","target":"1_node","sourceType":"TopCenter","targetType":"TopCenter"},{"source":"0_node","target":"1_node","sourceType":"BottomCenter","targetType":"LeftMiddle"}]},{"id":2}]');

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE IF NOT EXISTS `boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `boards`
--

INSERT INTO `boards` (`id`, `userid`, `name`) VALUES
(21, 2, 'Chris Flow'),
(22, 2, 'sup'),
(23, 2, 'new');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first` varchar(50) NOT NULL,
  `last` varchar(50) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first`, `last`, `username`, `password`, `email`) VALUES
(2, 'Chris', 'Mungol', 'cmungol', '202cb962ac59075b964b07152d234b70', 'cmungol@gmail.com');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `boarddata`
--
ALTER TABLE `boarddata`
  ADD CONSTRAINT `boarddata_ibfk_1` FOREIGN KEY (`boardid`) REFERENCES `boards` (`id`);

--
-- Constraints for table `boards`
--
ALTER TABLE `boards`
  ADD CONSTRAINT `boards_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
