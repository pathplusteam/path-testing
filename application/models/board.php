<?php
    class Board extends CI_Model {
        function grabBoards($id)
        {
            $this -> db -> select('id, name');
            $this -> db -> from('boards');
            $this -> db -> where('userid = ' . "'" . $id . "'");
            //$this -> db -> where('password = ' . "'" . md5($password) . "'");
            //$this -> db -> limit(1);
            
            $query = $this -> db -> get();
            
            if($query -> num_rows() >= 1)
            {
                return $query->result();
            }
            else
            {
                return false;
            }
        }
        
        function createBoards($id, $name)
        {
            $data = array('userid' => mysql_real_escape_string($id), 
                          'name' => mysql_real_escape_string($name));
            $this->db->insert('boards', $data);
        }
        
        function isBoardAuthorizedForUser($username, $id)
        {
            $this -> db -> select('name');
            $this -> db -> from('boards');
            $this -> db -> join('users', 'boards.userid = users.id', 'inner');
            $this -> db -> where('users.username = ' . "'" . $username . "'" . ' AND ' . 'boards.id=' . $id);
            
            $query = $this -> db -> get();
            
            if($query -> num_rows() >= 1)
            {
                return $query->result();
            }
            else
            {
                return false;
            }
        }
        
        //does the board contain nodes?
        function isBoardEmpty($username, $id)
        {
            $this -> db -> select('boardid');
            $this -> db -> from('boarddata');
            $this -> db -> join('boards', 'boarddata.boardid = boards.id', 'inner');
            $this -> db -> join('users', 'boards.userid = users.id', 'inner');
            $this -> db -> where('users.username = ' . "'" . $username . "'" . ' AND ' . 'boarddata.boardid=' . $id);
            
            $query = $this -> db -> get();
            
            if($query -> num_rows() >= 1)
            {
                return false;
            }
            else
                return true;
        }
        
        function saveBoard($data, $id)
        {
            
            $this -> db -> select('boardid');
            $this -> db -> from ('boarddata');
            $this -> db -> where('boarddata.boardid = ' . "'" . $id . "'");
            
            $query = $this -> db -> get();
            
            if($query -> num_rows() >= 1)
            {
                //update 
                $data = array('boardid' => $id, 'data' => $data);
                $this -> db -> where('boardid', $id);
                $this -> db -> update('boarddata', $data);
            }
            else {
                
                //insert 
                
                $data = array('boardid' => $id, 'data' => $data);
                $this -> db -> insert('boarddata', $data);
            }
            return true;
        }
        
        function grabData($id)
        {
            $this -> db -> select('data');
            $this -> db -> from ('boarddata');
            $this -> db -> where('boarddata.boardid = ' . "'" . $id . "'");
            
            $query = $this -> db -> get();
            
            return $query -> result();
        }
    }
?>
