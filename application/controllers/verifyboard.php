<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

      class VerifyBoard extends CI_Controller {

          function __construct()
          {
              parent::__construct();
              $this->load->model('board','',TRUE);
              
              //This method will have the credentials validation
              $this->load->library(array('form_validation', 'session'));
              //$this->load->library('form_validation');
              //$this->load->library('session');
              $this->load->helper('url'); 
          }

          function index()
          {
              //$this->form_validation->set_rules('name', 'BoardName', 'trim|required|xss_clean|callback_check_database');
              $this->form_validation->set_rules('name', 'BoardName', 'trim|required|xss_clean|callback_check_database');

              if($this->form_validation->run() == FALSE)
              {
                  //Field validation failed.&nbsp; User redirected to dashboard page
                  $session_data = $this->session->userdata('logged_in');
                  $data['username'] = $session_data['username'];
                  
                  //$this->load->view('dashboard_view', $data);
                  
                  //grab the boards for the user
                  $result = $this->board->grabBoards($session_data['id']);

                  redirect('dashboard');
              }
              else
              {
                  //Go to private area
                  redirect('dashboard');
              }
          }

          function check_database($name)
          {
              //Field validation succeeded.&nbsp; Validate against database
              $name = $this->input->post('name');
              $session_data = $this->session->userdata('logged_in');
              
              //query the database
              $this->board->createBoards($session_data['id'], $name);
              
              return true;
              /*
              if($result)
              {
                  $sess_array = array();
                  foreach($result as $row)
                  {
                      $sess_array = array(
                        'id' => $row->id,
                        'username' => $row->username
                      );
                      $this->session->set_userdata('logged_in', $sess_array);
                  }
                  return TRUE;
              }
              else
              {
                  $this->form_validation->set_message('check_database', 'Invalid username or password');
                  return false;
              }
              */
          }
      }
?>

