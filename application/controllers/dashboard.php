<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
      //session_start(); //we need to call PHP's session object to access it through CI
      class Dashboard extends CI_Controller {

          function __construct()
          {
              parent::__construct();

              //Load Libraries
              $this->load->library(array('form_validation', 'session'));
              $this->load->helper(array('url')); 
              
              //load the board model
              $this->load->model('board','',TRUE);
          }

          function index()
          {
              if($this->session->userdata('logged_in'))
              {
                  $session_data = $this->session->userdata('logged_in');
                  $data['username'] = $session_data['username'];
                  
                  //$this->load->view('dashboard_view', $data);
                  
                  //grab the boards for the user
                  $result = $this->board->grabBoards($session_data['id']);
                  
                  $nameArray = [];
                  $idArray = [];

                  if($result) {
                      
                      $data['boardsExist'] = true;
                      
                      foreach($result as $row)
                      {
                          $nameArray[] = $row->name;
                          $idArray[] = $row->id;
                      }
                  }
                  else {
                      $data['boardsExist'] = false;
                  }
                  
                  $data['nameArray'] = $nameArray;
                  $data['idArray'] = $idArray;
                  
                  $this->load->view( 'dashboard_view', $data);
              }
              else
              {
                  //If no session, redirect to login page
                  redirect('login', 'refresh');
              }
          }
      }
?>