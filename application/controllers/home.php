<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
      //session_start(); //we need to call PHP's session object to access it through CI
      class Home extends CI_Controller {

          function __construct()
          {
              parent::__construct();

              //Load Libraries
              //$this->load->library(array('form_validation', 'session'));
              //$this->load->helper(array('url', 'html')); 
              
              //load the board model
              $this->load->model('board','',TRUE);
          }

          function index()
          {
              if($this->session->userdata('logged_in'))
              {
                  $session_data = $this->session->userdata('logged_in');
                  $data['username'] = $session_data['username'];
                  $this->load->view('home_view', $data);
              }
              else
              {
                  //If no session, redirect to login page
                  redirect('login', 'refresh');
              }
          }
          
          function board($index)
          {
              if($this->session->userdata('logged_in'))
              {
                  $session_data = $this->session->userdata('logged_in');
                  
                  //check if user is authorized to access board
                  $result = $this->board->isBoardAuthorizedForUser($session_data['username'], $index);
                  if($result)
                  {
                      //load the specific board
                      $session_data = $this->session->userdata('logged_in');
                      $data['username'] = $session_data['username'];
                      
                      foreach($result as $row)
                      {
                        $data['boardName'] = $row->name;
                      }
                      
                      $data['isBoardEmpty'] = $this->isEmpty($session_data['username'], $index);
                      $data['boardId'] = $index;
                      
                      $this->load->view('home_view', $data);
                  }
                  else
                  {
                      //if no access redirect to dashboard
                      redirect('dashboard', 'refresh');
                  }
                  
              }
              else
              {
                  redirect('login', 'refresh');
              }
          }

          function logout()
          {
              $this->session->unset_userdata('logged_in');
              session_destroy();
              redirect('welcome', 'refresh');
          }
          
          private function isEmpty($username, $id)
          {
               if($this->board->isBoardEmpty($username, $id))
                  {
                      return json_encode('true');
                  }
                  else
                  {
                      return json_encode('false');
                  }
          }

          function saveData()
          {
              if($this->session->userdata('logged_in'))
              {
                  $result = $this->board->saveBoard($this->input->post('data'), $this->input->post('id'));
                  echo $result;
                  
              }
              else
              {
                  redirect('login', 'refresh');
              }
          }
          
          function loadData($id)
          {
              $result = $this->board->grabData($id);
              foreach($result as $row)
              {
                  echo $row->data;
              }
          }
      }
?>

