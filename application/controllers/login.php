
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

      class Login extends CI_Controller {

          function __construct()
          {
              parent::__construct();
              $this->load->helper(array('form', 'url'));

          }

          function index()
          {
              //$this->load->helper(array('form', 'url'));
              $page='login_view';
              
              $data['title'] = ucfirst($page);
              $this->load->view('templates/header', $data);
              $this->load->view( $page, $data);
              $this->load->view('templates/footer', $data);
          }
      }
?>