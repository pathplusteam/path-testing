
<!-- Declare HTML 5 -->
<!DOCTYPE html> 

<html>
    <head>
    <!-- UTF-8 -->
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!-- css for the main program -->
    <?php 
        
        echo link_tag('css/index.css'); 
        echo link_tag('css/jquery.contextMenu.css');
        echo link_tag('css/jquery-ui-1.8.24.custom.css');
        echo link_tag('css/reveal.css');
    ?>

    <link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:700,400' rel='stylesheet' type='text/css'>


            <title>Path Plus Development</title>

    <!-- toggle ability -->
    <script type="text/javascript">
                function toggleDiv(divId) {
                    $("#" + divId).toggle("fast");
                }
                
                <?php echo 'var boardId = ' . $boardId . ';';
                      echo 'var isBoardEmpty = ' . $isBoardEmpty . ';'; ?>
    </script>
    </head>
<body>

        <div style="position:relative;">
            <div id="canvas"></div>
	 </div>

        <!-- 
            The top "main" menu panel
        -->

        <div id="topMenu" class="panel" style="width: 1028px; height: 50px; top: 20px; left: 50px; position: absolute;">
            <div id="topMenuLogin" style="float: right;">
                <p><?php echo 'Welcome: ' . $username . ' ';  echo anchor('home/logout/', '[Logout]' ); ?></p>
                <p> Viewing Board: <?php echo $boardName?> </p>
            </div>
        </div>

        <!-- 
            Left Panel
            #leftPanel
        -->
        <div id="elementPanel" class="panel">
            ELEMENT PANEL
            <!-- Show/Hide -->
            <div id="showHide">
                <a href="javascript:toggleDiv('elementPanel_b');">Show/Hide</a>
            </div>
            <div id="elementPanel_b">
            <!--<img class="squareGhost" src="images/square.gif" />-->
            <img class="squareReal" src="http://localhost/path-testing/images/square.gif" />
            <div id="testButton"><p>Testing</p></div>
            <div id="deleteNodeButton"><p>Delete Nodes</p></div>
            <div id="save">Save</div>
            <!--<div id="path"><p>Click Me</p></div>-->
        </div>
        </div>

        <!-- 
            Log panel
        -->
        <div id="loggingPanel" class="panel">
            Nodes
            <div id="showHide">
                <a href="javascript:toggleDiv('loggingPanel_b');">Show/Hide</a>
            </div>
            <div id="loggingPanel_b" style="height: 300px; overflow: auto;">
                <p></p>
            </div>
        </div>
        
        <!--
            Path panel
        -->
        <script type="text/javascript">
        </script>
        
        <div id="debug" class="panel">
            Paths
            <div id="showHide"><a href="javascript:toggleDiv('debug_b');">Show/Hide</a></div>
            <div id="debug_Update"><a href="javascript:PrintConnections();">Update Paths</a></div>
            <div id="debug_b">
                <div id="paths">
                </div>
            </div>
        </div>

        <!--
            Loading Modal
        -->
        <div id="loadingModal" class="reveal-modal">
            <h1>Loading... Please Wait</h1>
            <a class="close-reveal-modal">&#215;</a>
        </div>

        <!-- 
            Meta Data
        -->
        <div id="metaData">
            <form id='metaForm'>
                <ul>
                    <li>
                        <input type='radio' name='enumerated' value='start' /><span>Start</span>
                        <input type='radio' name='enumerated' value='end' /><span>End</span>
                        <input type='radio' name='enumerated' value='none' /><span>None</span>
                    </li>

                    <li>
                        <label for="name">Name:</label>
                        <input type="text" name="name" />
                    </li>

                    <li>
                        <label for="message">Description:</label>
                        <textarea id="description" name="message" cols="40" rows="6" required=""></textarea>
                    </li>
                    <li id="break1"></li>
                </ul>
            </form>
        </div>

        <!--
            Collapsible Panels
        -->
        <div class="ui-widget collapse main">
            <div id="expander-demo-control" class="script-header">
                <span id="script-triangle" class="ui-icon ui-expander floatLeft">+</span>
                <span id="script-name" ></span>
            </div>
            <div id="expander-demo" class="ui-widget-content">
                <textarea id="precondition" name="message" cols="40" rows="6" required=""></textarea>
                <textarea id="body" name="message" cols="40" rows="6" required=""></textarea>
            </div>
        </div>

        <!-- 
             * JQuery
             * JQuery UI
             * JSPlumb
             * Main *newCreation

             * Context Menu
            <script></script> tag is new with HTML5
         -->        
        <script src="http://localhost/path-testing/js/jquery-1.8.0.min.js"></script>
        <script src="http://localhost/path-testing/js/jquery.reveal.js"></script>
        <script src="http://localhost/path-testing/js/jquery-ui-1.8.24.custom.min.js"></script>
        <script src="http://localhost/path-testing/js/jquery.jsPlumb-1.3.14-all.js"></script>
        <script src="http://localhost/path-testing/js/jquery.contextMenu.js"></script>
        <script src="http://localhost/path-testing/js/newCreation.js"></script>
        <script src="http://localhost/path-testing/js/json2.js"></script>
	</body>
</html>

