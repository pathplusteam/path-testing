<!-- Declare HTML 5 -->
<!DOCTYPE html> 

<html>
    <head>
    <!-- UTF-8 -->
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!-- css for dashboard -->
    <link rel="stylesheet" type="text/css" href="css/dashboard.css" /> 
    
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.24.custom.css" />
    
    <link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:700,400' rel='stylesheet' type='text/css'>
    
    <title>Dashboard</title>
    
    <script src="js/jquery-1.8.0.min.js"></script>
    <script src="js/jquery-ui-1.8.24.custom.min.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            //add createBoard modal
            $('#addBoardForm')
                .dialog({
                        autoOpen: false,
                        height: 300,
                        width: 350,
                        modal: true,
                        buttons: {
                            Add: function() {
                                $('#createBoardForm').submit();
                                $(this).dialog( "close" );
                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                });
                
            //open createBoard modal
            $('#createBoard').click(function() {
               $('#addBoardForm').dialog("open"); 
            });
            var nameArray = [];
            <?php 
                $count = 0;
                foreach($nameArray as $a)
                {
                    echo "$('#createBoard').clone().addClass('$idArray[$count]').appendTo($('#dashboard_contents')).find($('p#add')).html('" . anchor('home/board/'.$idArray[$count], strip_quotes($a)) . "');";
                    $count++;
                }
               
            ?>
            console.log(nameArray);
            
        });
    </script>
    </head>
<body>

    <div id="dashboard_contents">
        <hr>
            <h1>Dashboard</h1>
            <?php echo 'welcome ' . $username . '!'; ?> <a href="home/logout">[Logout]</a> <br>
            <?php echo validation_errors(); ?>
        <hr>
        
        <div id="createBoard">
            <p id="symbol">+</p>
            <p id="add">Add</p>
        </div>
    </div>
    
    <div id="addBoardForm">
        
        <?php 
        $attributes = array('id' => 'createBoardForm');
        echo form_open('verifyboard', $attributes); ?>
            <fieldset>
                <label for="Name">Name of Board:</label>
                <input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
                <input type="submit" name="submitCreateBoardForm" value="Add" />
            </fieldset>
        </form>
        
    </div>
    <div class="result"></div>
    
</body>
</html>