﻿//Globals
var background = null;

(function ($) {

    var nodeArray;
    var selectArray = [];

    $(document).ready(function () {

        /***********************************
            Tracking
        ************************************/

        /* Drop Event Position */
        var dropEventMouseX = 0;
        var dropEventMouseY = 0;

        /* Node */
        var xl = 0;             //the x coordinate of the top left corner of rect
        var yl = 0;             //the y coordinate of the top left corner of rect
        var side = 50;         //100px width and height
        var numberOfNodes= 0;  //Track number of nodes

        nodeArray = [];

        //track canvas stats
        var canvasWidth = 1150;
        var canvasHeight = 800;
        var top = 120;

        //create the paper (pretty much the canvas)
        var paper = Raphael("myCanvas", canvasWidth, canvasHeight);

        //create a background
        background = paper.rect(0, 0, canvasWidth, canvasHeight).attr({ fill: "white" });


        /*
            c1 = paper.circle(50, 20, 10),
            c2 = paper.circle(250, 25, 10);
    
            c1.joint(c2);
        */

        /***********************************
            Events
        ************************************/

        //handle drag
        $('#leftPanel img.squareReal').draggable({
            revert: true,
            revertDuration: .1
        });

        //debug : output mouse position in window document
        $(document).mousemove(function (e) {
           // console.log("documentX: " + e.pageX + "," + "documentY: " + e.pageY);
        });

        //handle drop
        var dropCompleted = false;
        $('#myCanvas').droppable({
            drop: function (event, ui) {

                //grab the x,y position of the mouse of drop
                dropEventMouseX = event.pageX;
                dropEventMouseY = event.pageY;
                console.log(dropEventMouseX + " " + dropEventMouseY);
                //correct the actual drop position
                var xl = dropEventMouseX - (side / 2);
                var yl = dropEventMouseY - (side / 2) - top;

                //create the new Node
                var node = new Node(paper, {
                    xl: xl,
                    yl: yl,
                    width: side,
                    height: side,
                    fill: "#68838B",
                    id: numberOfNodes
                });

                node.draw();

                //dynamically add Node
                nodeArray.push(node);

                //update NumberOfNodes
                numberOfNodes++;

                //debug : note when a node has been created and dropped into canvas
                //console.log("I dropped it in canvas");
                printNodes();
            }
        });

        $("#path p").click(function () {
            createPaths(selectArray);
        });
    });

    $(document).keydown(function (event) {
        if (event.keyCode == 68) { // d 
            for (var i = 0; i < nodeArray.length; ++i) {
                console.log(nodeArray[i].getNodeSelected());
                if (nodeArray[i].getNodeSelected()) {

                    //remove Node from paper
                    nodeArray[i].getNode().remove();
                    //remove selector attached to Node from paper
                    nodeArray[i].getSelector().getSelector().remove();
                    //remove text attached to Node from paper
                    nodeArray[i].getText().remove();
                    //remove element from array
                    nodeArray.splice(i, 1);

                    numberOfNodes = nodeArray.length;
                }
            }
        }
    });

    function printNodes() {
        for (var i = 0; i < nodeArray.length; ++i) {
            console.log("Paper added: " + nodeArray[i].getID());
        }
    }

    function createPaths() {

        /*
        for (var i = 0; i < nodeArray.length; ++i) {
            if (nodeArray[i].getNodeSelected()) {
                selectArray.push(nodeArray[i]);
            }
        }

        for (var i = 0; i < selectArray.length - 1; ++i) {
            selectArray[i].getNode().joint(selectArray[i + 1].getNode());
        }

        selectArray = [];

        console.log(selectArray);
        */
    }
} (jQuery));