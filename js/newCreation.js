//Global Scope

//nodeArray
var nodeArray = [];

//linkArray
var linkArray = [];

//logger
var logger = new Logger("#loggingPanel_b p:last");

//fix firing twice
var fire = 0;

//are more than one node selected?
var isMultipleNodesSelected = false;

//Start node
var startNode = "";

//End node
var endNode = "";

//track 
var id = 0;

var idThatOpenedModal = -1;

(function ($) {

    $(document).ready(function () {
        
        console.log('isBoardEmpty ' + isBoardEmpty);

        $('#loadingModal').reveal({
                animation: 'fadeAndPop',                   //fade, fadeAndPop, none
                animationspeed: 300,                       //how fast animtions are
                closeonbackgroundclick: false,              //if you click background will modal close?
                dismissmodalclass: 'close-reveal-modal'    //the class of a button or element that will close an open modal
            });

        $('.close-reveal-modal').bind('click');

        

        /////////////////////////////////////////////
        //
        //             Loading Flowchart
        //
        ////////////////////////////////////////////
        if(isBoardEmpty == 'false')
        {

            //open the loading modal
            $('#loadingModal').reveal();

            //loading board
             $.getJSON('http://localhost/path-testing/home/loadData/'+boardId,
                    function(result) {

                        console.log('result ' + result);
                        console.log('links ' + result[1].links.length);
                        //load the data from the server
                        if(result[0].nodes.length != 0) {

                            //load nodes
                            for(var i=0; i < result[0].nodes.length; ++i)
                            {
                                var node = new Node(result[0].nodes[i].css, result[0].nodes[i].realId);
                                node.createNode();
                                node.setScript(result[0].nodes[i].script);
                                node.setStart(result[0].nodes[i].start);
                                node.setEnd(result[0].nodes[i].end);
                                node.setDescription(result[0].nodes[i].description);
                                nodeArray.push(node);
                                //id = result[result.length-1];
                            }

                            //continue the node id count
                            id = result[2].id;
                        }

                        if(result[1].links.length != 0) {
                            //load links
                            var connectorPaintStyle = {
                                    lineWidth: 3,
                                    strokeStyle: "#346789",
                                    joinstyle: "round",
                                    outlineColor: "white",
                                    outlineWidth: 5
                                };

                            for(var i=0; i < result[1].links.length; ++i)
                            {
                                jsPlumb.connect({
                                source: result[1].links[i].source,
                                target: result[1].links[i].target,
                                paintStyle: connectorPaintStyle,
                                anchors:[result[1].links[i].sourceType, result[1].links[i].targetType],
                                endpointStyle: { fillStyle: "#225588", radius: 4, outlineColor: "none" }
                                });
                            }

                            linkArray = result[1].links.slice();
                        }

                        $('.close-reveal-modal').trigger('click');
                    });
        }
        else
        {
            id = 0;  
            //close the loading modal
            $('.close-reveal-modal').trigger('click');
        }
            
        //add Modal support for Node
        createDialog();
        
        //save logic
        $('#save').click(function(){
           saveState(); 
        });
        
        // Draggable Classes
        //containment: within document
        $(".panel").draggable({
            containment: "document",
            start: function () {
                // if we're scrolling, don't start and cancel drag
                if ($(this).data("scrolled")) {
                    $(this).data("scrolled", false).trigger("mouseup");
                    return false;
                }
            }
        }).find("*").andSelf().scroll(function () {
            // bind to the scroll event on current elements, and all children.
            // we have to bind to all of them, because scroll doesn't propagate.

            //set a scrolled data variable for any parents that are draggable, so they don't drag on scroll.
            $(this).parents(".ui-draggable").data("scrolled", true);
        });
        
        /////////////////////////////////////////////
        //
        //             Adding Nodes
        //
        ////////////////////////////////////////////

        //real image is draggable
         //containment: within document
        $('#elementPanel img.squareReal').draggable({
            revert: true,
            revertDuration: .1,
            containment: "document",
            stop: function (event, ui) {

                var css = {
                    left: event.pageX,
                    top: event.pageY,
                    //border: "2px solid red",
                    width: "50px",
                    height: "20px",
                    position: "absolute",
                    //cursor: "pointer",
                    "z-index" : "20"
                }

                //create node, add it to document
                var node = new Node(css, id);
                node.createNode();

                //add reference to array
                nodeArray.push(node);

                //log add
                logger.logAddNode(node.getId());
                
                //increment id 
                id++;
                
                //debug
                console.log("nodeArray " + nodeArray);
            }
        });
        
        /////////////////////////////////////////////
        //
        //             Selectable
        //
        ////////////////////////////////////////////

        //make elements selectable
        $("#canvas").selectable({
            //when elements are unselected...
            unselected: function (event, ui) {
                isMultipleNodesSelected = false;
                //...deactivate the context menu
                disableEveryContextMenu();
            },
            selected: function (event, ui) {
                if($('.node.ui-selected').length > 1)
                    isMultipleNodesSelected = true;
                else
                    isMultipleNodeSelected = false;
            }
        });

        //debug
        $("#testButton").click(function () {
            //console.log(nodeArray[1].getOutEdgeArray());
            //console.log(nodeArray[0], nodeArray[1]);

            // the definition of source endpoints (the small blue ones)
            // this is the paint style for the connecting lines..
            jsPlumb.select({source:"0_node"}).each(function(connection) {  });
        });

            

        /////////////////////////////////////////////
        //
        //             Delete Nodes
        //
        ////////////////////////////////////////////
        
        //delete selected nodes
        $("#deleteNodeButton").click(function () {
            if (nodeArray.length > 0) {
                deleteNodes();
            }
        });

        /////////////////////////////////////////////
        //
        //             jsPlumbConnection
        //
        ////////////////////////////////////////////
        //connection event
        jsPlumb.bind("jsPlumbConnection", function (info) {
            if (fire < 1) {
                logger.logAddConnection(info.sourceId, info.targetId);
                linkArray.push({source: info.sourceId, 
                                target: info.targetId,
                                sourceType: info.sourceEndpoint.anchor.type,
                                targetType: info.targetEndpoint.anchor.type });
                fire++;
            }
            else
                //reset fire
                fire = 0;

            console.log(linkArray);
        });

        jsPlumb.bind("jsPlumbConnection", function (info) {
            if (fire < 1) {
                logger.logAddConnection(info.sourceId, info.targetId);
                linkArray.push({source: info.sourceId, 
                                target: info.targetId,
                                sourceType: info.sourceEndpoint.anchor.type,
                                targetType: info.targetEndpoint.anchor.type });
                fire++;
            }
            else
                //reset fire
                fire = 0;
        });

        //detachment event
        jsPlumb.bind("jsPlumbConnectionDetached", function (info) {
            if (fire < 1) {
                logger.logDeleteConnection(info.sourceId, info.targetId);
                var searchForObject = {source: info.sourceId, 
                                       sourceType: info.sourceEndpoint.anchor.type, 
                                       target: info.targetId,
                                       targetType: info.targetEndpoint.anchor.type };
                linkArray.splice(linkArray.indexOf(searchForObject), 1);
                fire++;
            }
            else
                //reset fire
                fire = 0;
        });
        
        //detachment event
        jsPlumb.bind("jsPlumbConnectionDetached", function (info) {
            if (fire < 1) {
                logger.logDeleteConnection(info.sourceId, info.targetId);
                var searchForObject = {source: info.sourceId, 
                                       sourceType: info.sourceEndpoint.anchor.type,
                                       target: info.targetId,
                                       targetType: info.targetEndpoint.anchor.type };
                linkArray.splice(linkArray.indexOf(searchForObject), 1);
                fire++;
            }
            else
                //reset fire
                fire = 0;
        });
    });

    /////////////////////////////////////////////
    //
    // jQuery Extension - Collapsible Panels
    // http://www.leghumped.com/blog/2011/11/26/jquery-collapsible-panel-plugin/
    //
    ////////////////////////////////////////////
    $.fn.extend({
        collapsiblePanel: function () {
            $(this).each(function () {
                var indicator = $(this).find('.ui-expander').first();
                var header = $(this).find('.script-header').first();
                var content = $(this).find('.ui-widget-content').first();
                if (content.is(':visible')) {
                    indicator.removeClass('ui-icon-triangle-1-e ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-e');
                } else {                                       
                    indicator.removeClass('ui-icon-triangle-1-e ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-s');
                }
 
                header.click(function () {
                    content.slideToggle(500, function () {
                        if (content.is(':visible')) {
                            indicator.removeClass('ui-icon-triangle-1-e ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-e');
                        } else {                                       
                            indicator.removeClass('ui-icon-triangle-1-e ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-s');
                        }
                    });
                });
            });
        }
    });
}(jQuery));

/////////////////////////////////////////////
//
//              NODE CLASS
//
////////////////////////////////////////////

function Node(css, id)
{
    this.css = css;
    this.id = id + "_node";
    this.realId = id;
    this.outEdgeArray = [];

    //form
    this.name = this.id;
    this.description = '';
    this.script = {};
    
    //start and end node
    this.start = false;
    this.end = false;
}

Node.prototype.getId = function () {
    return this.id;
}

Node.prototype.getRealId = function () {
    return this.realId;
}

Node.prototype.setName = function (name) {
    this.name = name;
}

Node.prototype.setScript = function (script) {
    this.script = script;
}

Node.prototype.setStart = function (start) {
    this.start = start;
}

Node.prototype.setEnd = function (end) {
    this.end = end;
}

Node.prototype.setDescription = function (description) {
    this.description = description;
}

Node.prototype.createNode = function() {
    var Node = this;

    var node_name = "<p>" + Node.id + "</p>";

    //self-closed element
    var divNode = jQuery('<div/>', {
        id: Node.id,
        class: "node",
        css: Node.css
    }).html(node_name).appendTo($("#canvas"));

    //mousedown event
    $("#" + Node.id).mousedown(function () {
        //...make the created node selected
        $("#" + Node.id).addClass("ui-selected");

        enableContextMenu(Node.id);

        if($('.node.ui-selected').length > 1 && isMultipleNodesSelected) {
            //do nothing
        }
        else {
              //...deselect other nodes
              for (var i = 0; i < nodeArray.length; ++i) {
                if (nodeArray[i].getId() == Node.id) {
                    //do nothing
                } else {
                    //deselect the other nodes
                    $("#" + nodeArray[i].getId()).removeClass("ui-selected");
                    disableContextMenu(nodeArray[i].getId());
                }
        }
        }
    });
    
    //double click event -- brings up the dialog
    $("#" + Node.id).dblclick(function () {
        //metaData should contain all the information;

        //set the id that called this function
        idThatOpenedModal = Node.id;

        console.log(nodeArray);

        //load the start and end values, respectively
        $("#metaForm input[name=enumerated]:radio:nth(0)").attr('checked', Node.start);
        $("#metaForm input[name=enumerated]:radio:nth(1)").attr('checked', Node.end);
        if(Node.start == false && Node.end == false)
        {
            $("#metaForm input[name=enumerated]:radio:nth(2)").attr('checked', true);
        }

        //load the Node name
        $("#metaForm input[name=name]").val(Node.name);


        //load the description
        $("#metaForm textarea#description").val(Node.description);

        //load the scripts
        //count the number of outEdges
        var count = 0;
        jsPlumb.select({source:Node.id})
               .each(function(connection) {
                             var take = $('.collapse.main').clone().removeClass('main').addClass('scripts').insertBefore('#break1');
                             take.find('#script-name').html(connection.sourceId + "-->" + connection.targetId);
                             if(!jQuery.isEmptyObject(Node.script))
                             {
                                if(typeof Node.script.array[count] != 'undefined'){
                                    take.find('textarea#precondition').val(Node.script.array[count].precondition);
                                    take.find('textarea#body').val(Node.script.array[count].body);
                                    count++;
                                }
                             }
                             //take.find('textarea#script').html((typeof Node.script.array[count] === 'undefined') ? Node.script.array[count] : "");
               });

        //debug
        console.log(nodeArray[0]);

        /////////////////////////////////////////////
        //
        //             Collapsible Panels
        //
        ////////////////////////////////////////////
        $('.collapse').collapsiblePanel();

        //set name
        $( "#metaData" ).dialog( "open" );
    });

    /////////////////////////////////////////////
    //
    //              jsPlumb
    //
    ////////////////////////////////////////////

    // this is the paint style for the connecting lines..
    var connectorPaintStyle = {
        lineWidth: 3,
        strokeStyle: "#346789",
        joinstyle: "round",
        outlineColor: "white",
        outlineWidth: 5
    },
    // .. and this is the hover style. 
    connectorHoverStyle = {
        lineWidth: 5,
        strokeStyle: "#2e2aF8"
    },
    // the definition of source endpoints (the small blue ones)
    sourceEndpoint = {
        endpoint: "Dot",
        paintStyle: { fillStyle: "#225588", radius: 4 },
        isSource: true,
        connector: ["Flowchart", { stub: [40, 60], gap: 5 }],
        connectorStyle: connectorPaintStyle,
        connectorHoverStyle: connectorHoverStyle,
        overlays: [
                    ["Label", {
                        location: [0.5, 1.5],
                        cssClass: "endpointSourceLabel"
                    }]
        ]
    },
    // a source endpoint that sits at BottomCenter
    // bottomSource = jsPlumb.extend( { anchor:"BottomCenter" }, sourceEndpoint),
    // the definition of target endpoints (will appear when the user drags a connection) 
    targetEndpoint = {
        endpoint: ["Dot", /*{ width:10, height:10}*/],
        paintStyle: { fillStyle: "#225588", radius: 4 },
        maxConnections: -1,
        isTarget: true,
        overlays: [
                    ["Label", { location: [0.5, -0.5], cssClass: "endpointTargetLabel" }]
        ]
    };

    jsPlumb.importDefaults({
        // default drag options
        DragOptions: { cursor: 'pointer', zIndex: 2000 },
        // default to blue at one end and green at the other
        EndpointStyles: [sourceEndpoint.paintStyle, targetEndpoint.paintStyle],
        // blue endpoints 7 px; green endpoints 11.
        Endpoints: [sourceEndpoint.endpoint, targetEndpoint.endpoint],
        // the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
        // case it returns the 'labelText' member that we set on each connection in the 'init' method below.
        ConnectionOverlays: [
            ["Arrow", { location: 0.9 }]
        ],
        ConnectorZIndex: 5,
        PaintStyle: connectorPaintStyle,
        HoverPaintStyle: connectorHoverStyle,
        Connector: 'Flowchart'
    });

    //jsPlumb.Defaults.Container = $(divNode);
    /*
    var allSourceEndpoints = [], allTargetEndpoints = [];
    _addEndpoints = function (toId, sourceAnchors, targetAnchors) {
        for (var i = 0; i < sourceAnchors.length; i++) {
            var sourceUUID = toId + sourceAnchors[i];
            allSourceEndpoints.push(jsPlumb.addEndpoint(toId, sourceEndpoint, { anchor: sourceAnchors[i], uuid: sourceUUID }));
        }
        for (var j = 0; j < targetAnchors.length; j++) {
            var targetUUID = toId + targetAnchors[j];
            allTargetEndpoints.push(jsPlumb.addEndpoint(toId, targetEndpoint, { anchor: targetAnchors[j], uuid: targetUUID }));
        }
    };
    */

    //jsPlumb.addEndpoint(divNode, sourceEndpoint, { anchor: "Continuous", paintStyle: { fillStyle: "red" } });
    //jsPlumb.addEndpoint(divNode, targetEndpoint, { anchor: "Continuous", paintStyle: { fillStyle: "red" } });
    console.log(divNode);
    jsPlumb.addEndpoint(divNode, targetEndpoint, { anchor: "TopCenter"});
    jsPlumb.addEndpoint(divNode, sourceEndpoint, { anchor: "TopCenter"});

    jsPlumb.addEndpoint(divNode, targetEndpoint, { anchor: "LeftMiddle"});
    jsPlumb.addEndpoint(divNode, sourceEndpoint, { anchor: "LeftMiddle"});

    jsPlumb.addEndpoint(divNode, targetEndpoint, { anchor: "RightMiddle"});
    jsPlumb.addEndpoint(divNode, sourceEndpoint, { anchor: "RightMiddle"});

    jsPlumb.addEndpoint(divNode, targetEndpoint, { anchor: "BottomCenter"});
    jsPlumb.addEndpoint(divNode, sourceEndpoint, { anchor: "BottomCenter"});


    //_addEndpoints("window4", ["TopCenter", "BottomCenter"], ["LeftMiddle", "RightMiddle"]);

    // make all the window divs draggable						
    //jsPlumb.draggable(jsPlumb.getSelector(".window"), { grid: [20, 20] });
    // THIS DEMO ONLY USES getSelector FOR CONVENIENCE. Use your library's appropriate selector method!
    //jsPlumb.draggable(jsPlumb.getSelector(".window"));

    //jsPlumb.repaintEverything();

    //make the node draggable
    jsPlumb.draggable(jsPlumb.getSelector(divNode), {
        containment: "document",
        start: function (evt, ui) {
        },
        //onCreate...
        create: function (event, ui) {
            //...make the created node selected
            $("#" + Node.id).addClass("ui-selected");
            
            //add contextMenu support for Node
            createContextMenu(Node.id);

            //...deselect other nodes
            for (var i = 0; i < nodeArray.length; ++i) {
                $("#" + nodeArray[i].getId()).removeClass("ui-selected");
                disableContextMenu(nodeArray[i].getId());
            }
        },
        drag: function (evt, ui) {
        }
    });
    // console.log(jsPlumb.getConnections()[0].sourceId);
}

/////////////////////////////////////////////
//
//              LOGGER CLASS
//
////////////////////////////////////////////

function Logger(selector) {
    this.loggerArray = [];
    this.selector = selector;
}

Logger.prototype.getLog = function () {
    return this.loggerArray;
}

Logger.prototype.logAddNode = function (id) {
    var Logger = this;
    var addMessage = "<p>" + "Added " + id + " to canvas" + "</p>";
    console.log(Logger.selector);
    $(Logger.selector).after(addMessage);
    Logger.loggerArray.push(addMessage);
}

Logger.prototype.logDeleteNode = function (id) {
    var Logger = this;
    var deleteMessage = "<p>" + "Deleted " + id + " from canvas" + "</p>";
    console.log(Logger.selector);
    $(Logger.selector).after(deleteMessage);
}

Logger.prototype.logAddConnection = function (sourceId, targetId) {
    var Logger = this;
    var connectionMessage = "<p>" + "Added connection: " + sourceId + " --> " + targetId + "</p>";
    console.log(fire);

    console.log(Logger.selector);
    $(Logger.selector).after(connectionMessage);
}

Logger.prototype.logDeleteConnection = function (sourceId, targetId) {
    var Logger = this;
    var connectionMessage = "<p>" + "Deleted connection: " + sourceId + " --> " + targetId + "</p>";
    console.log(fire);

    console.log(Logger.selector);
    $(Logger.selector).after(connectionMessage);
}


/////////////////////////////////////////////
//
//              Helper Functions
//
////////////////////////////////////////////

//delete
function deleteNodes() {
    if (nodeArray.length > 0) {
        //grab all elements that are selected
        var selected = $("div.ui-selected");

        //grab all nodes that are selected
        var nodeSelected = $("div.node.ui-selected");

        if (!(typeof $(selected).attr("id") === "undefined")) {
            //remove all endpoints from selected nodes
            $(selected).each(function (index, value) {
                jsPlumb.removeAllEndpoints($(this));
            });

            //remove all nodes from selected nodes
            $(selected).each(function (index, value) {
                ($(this).remove());
            });

            //remove all nodes from nodeArray from selected nodes
            $(nodeSelected).each(function (index, value) {
                for (var i = 0; i < nodeArray.length; ++i) {
                    if ($(this).attr("id") == nodeArray[i].getId()) {
                        logger.logDeleteNode($(this).attr("id"));
                        nodeArray.splice(i, 1);
                    }
                }
             });
            //console.log("deleteNodes is getting called");
        }
  }
}

function findIndexOfNodeInArray() {
    for(var i=0; i<nodeArray.length; ++i)
    {
        if(nodeArray[i].id == idThatOpenedModal)
        {
            return i;
        }
    }
}

function deactivateStartNodes(skipIndex) {

    for(var i=0; i<nodeArray.length; ++i)
    {
        if(i != skipIndex)
        {
            //nodeArray[i].start = (nodeArray[i].start === true) ? false : false;
            if(nodeArray[i].start === true)
            {
                nodeArray[i].start = false;
            }
            $("#metaForm input[name=enumerated]:radio:nth(2)").attr('checked', true);
        }
    }
}

function deactivateEndNodes(skipIndex)   {
    for(var i=0; i<nodeArray.length; ++i)
    {
        if(i != skipIndex)
        {
            //nodeArray[i].end = (nodeArray[i].end === true) ? false : false;
            if(nodeArray[i].end === true)
            {
                nodeArray[i].end = false;
            }
            $("#metaForm input[name=enumerated]:radio:nth(2)").attr('checked', true);
        }
    }
}

function removeAllScripts()
{
    $(".collapse.scripts").remove();
}

/////////////////////////////////////////////
//
//             Context Menu
//
////////////////////////////////////////////

//enable context menu
function createContextMenu(id) {
    
    $.contextMenu({
        selector: "#" + id,
        callback: function (key, options) {
            switch (key) {
                case "delete": {
                    deleteNodes();
                }
            }
        },
        items: {
            "delete": { name: "Delete", icon: "delete" }
            //"sep1": "---------",
        }
    });
}

function enableContextMenu(id) {
    $("#" + id).contextMenu(true);
}

//disable context menu
function disableEveryContextMenu() {
    for (var i = 0; i < nodeArray.length; ++i) {
        $("#" + nodeArray[i].getId()).contextMenu(false);

    }
}

function disableContextMenu(id) {
    $("#" + id).contextMenu(false);
}

/////////////////////////////////////////////
//
//             jQuery Dialog
//
////////////////////////////////////////////

function createDialog() {
    /*
    var html = {
        radio : "<div id='radio_group'><input type='radio' name=" + id + "_group" + " value='start'><span>Start</span>" 
                + "<input type='radio' name=" + id + "_group" + " value='end'><span>End</span>"
                +  "</div>",
        radio_selector : 'input[name=' + id + '_group' + ']:radio'
    }
    */    
    $('#metaData')
        .dialog({
                autoOpen: false,
                height: 500,
                width: 800,
                modal: true,
                buttons: {
                    Save: function() {
                        var nodeIndex = findIndexOfNodeInArray();

                        //console.log(values);
                        //save start and end node state
                        /*
                        nodeArray[nodeIndex].start = ($("#metaForm input[name=enumerated]:radio:checked:nth(0)").length == 0) ? false : true;

                        //if start is true update end as false
                        if(nodeArray[nodeIndex].start === true)
                        {
                            nodeArray[nodeIndex].end = false;
                            //deactivate the other start nodes
                            deactivateStartNodes(nodeIndex);
                        }
                        */  
                        

                        if($("#metaForm input[name=enumerated]:radio:nth(0)").attr("checked") === 'checked')
                        {
                            nodeArray[nodeIndex].start = true;
                            nodeArray[nodeIndex].end = false;
                            deactivateStartNodes(nodeIndex);    
                        }
                        else if ($("#metaForm input[name=enumerated]:radio:nth(1)").attr("checked") === 'checked')
                        {
                            nodeArray[nodeIndex].start = false;
                            nodeArray[nodeIndex].end = true;
                            deactivateEndNodes(nodeIndex);   
                        }
                        else {
                            nodeArray[nodeIndex].start = false;
                            nodeArray[nodeIndex].end = false;
                        }

                        /*
                        nodeArray[nodeIndex].end = ($("#metaForm input[name=enumerated]:radio:checked:nth(1)").length == 0) ? false : true;

                        //if start is true update end as false
                        if(nodeArray[nodeIndex].end === true)
                        {
                            nodeArray[nodeIndex].start = false;
                            //deactivate the other end nodes
                            deactivateEndNodes(nodeIndex);
                        }
                        */

                        console.log(nodeArray[nodeIndex].start);
                        console.log(nodeArray[nodeIndex].end);

                        //save name
                        nodeArray[nodeIndex].name = $("#metaForm input[name=name]").val();

                        //save desription
                        nodeArray[nodeIndex].description = $("#metaForm textarea#description").val();

                        //
                        var scriptArray = [];

                        console.log($('.collapse.scripts'));

                        $('.collapse.scripts').each(function() {
                            var scriptObj = {}
                            scriptObj.nodeId = nodeArray[nodeIndex].getId();
                            scriptObj.name = $(this).find('#script-name').html();
                            scriptObj.precondition = $(this).find('textarea#precondition').val();
                            scriptObj.body = $(this).find('textarea#body').val();
                            scriptArray.push(scriptObj);
                        });

                        nodeArray[nodeIndex].script.array = scriptArray;
                        scriptArray = [];
                        console.log(nodeArray[nodeIndex]);

                        /*
                        var scriptEntry = {};
                        scriptEntry[nodeArray[nodeIndex].getId()] = scriptArray;
                        scriptArray = [];
                        console.log(scriptEntry);
                        //saveState
                        //saveScriptState();
                        */

                        saveState();

                        removeAllScripts();



                        //close
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        removeAllScripts();
                        $( this ).dialog( "close" );
                    }
                }
        });
    /*
   //check if start and end nodes are set
   if(startNode === "") {
       $(html.radio_selector).each(function() {
           if($(this).attr('value') === "start") {
                $(this).attr('disabled', false);
           }
       });
   }
   
   if(endNode === "") {
       $(html.radio_selector).each(function() {
           if($(this).attr('value') === "end") {
                $(this).attr('disabled', false);
           }
       });
   }
        
   //bind the change event to input[name=node_n_group]:radio     
   $(html.radio_selector).change(function() {
      console.log("true"); 
   });
    */
}

/////////////////////////////////////////////
//
//             Saving
//
////////////////////////////////////////////

//save board state
function saveState() {
   
   console.log(nodeArray);
   console.log(linkArray);
   
   //update node's position
   for(var i=0; i<nodeArray.length; ++i)
   {
       //update left position
       nodeArray[i].css.left = $('#' + nodeArray[i].id).css('left');
       //update top position
       nodeArray[i].css.top = $('#' + nodeArray[i].id).css('top');
   }
   
    //add the id state to the array
    var completeArray = [];
    var saveNodeArray = [];
    var saveLinkArray = [];

    saveNodeArray = nodeArray.slice();
    saveLinkArray = linkArray.slice();

    completeArray.push({nodes: saveNodeArray }, 
                       {links: saveLinkArray },
                       {id : id });

    //convert completeArray to JSON
    var jsonData = JSON.stringify(completeArray);

    //save the node array
    $.post('http://localhost/path-testing/home/saveData/', 
        {'data': jsonData, 'id':boardId},
        function(result) {
           alert("save nodes successful " + result); 
        });
}

/////////////////////////////////////////////
//
//              PATH ANALYSIS FUNCTIONALITY
//
/////////////////////////////////////////////

// Print the paths into our path ui region
// as div elements.
function PrintConnections()
{
    $("#paths div").remove();
    //$("#paths span:last").after("<div><p></p></div>");
    // Just a test for adding paths to a list.
    // Will still use a lot of the inner loop code.
    for (i = 0; i < linkArray.length; ++i)
        {
            $("#paths").append(
            "<div class=\"pathlist ui-corner-all\">" +
            "<div class=\"pathlisthandle\"><span class='ui-icon ui-icon-carat-2-n-s'></span></div>" +
            "<p>Path " + linkArray[i].source + "-" + linkArray[i].target +
                "</p></div>"
            );
        }
}

// Handle some of the path ui stuff.
$(function() {
    // Make the list of paths sortable by creating an jqUI accordion region
    // and making the path list div elements sortable. To make them selectable
    // as well we have to define a specific region for the user to be able to
    // sort them, with the rest of the region being used for selection.
    $("#paths")
        .accordion({
            //header: "> div > p",
            active: false,
            collapsible: true
        })
        .sortable({
            axis: "y",
            handle: ".pathlisthandle"
        })
        // Make the path list selectable and sortable.
        .selectable()
//            .find(".pathlist")
//                .addClass("ui-corner-all")
//                .prepend("<div class='pathlisthandle'><span class='ui-icon ui-icon-carat-2-n-s'></span></div>");
    // Delete path.
    $("#deletePathButton").click(function () {});
});