﻿//constructor
function Node(paper, nArr) {

    //reference to canvas
    this.paper = paper;

    //Node properties
    this.nArr = nArr;

    //Selector properties
    this.sArr = {
        xl: nArr.xl,
        yl: nArr.yl,
        width: nArr.width,
        height: nArr.height
    };

    //selector variable, pass canvas and selecotr objects
    this.selector = new Selector(paper, this.sArr);

    //is node selected?
    this.nodeSelected = false;

    //node text
    this.text = null;

    //our node reference
    this.node = null;

    //link established
    this.linkEstablished = false;
}

Node.prototype.getID = function () {
    return this.nArr.id;
}

Node.prototype.getNodeSelected = function () {
    return this.nodeSelected;
}

Node.prototype.getNode = function () {
    return this.node;
}

Node.prototype.getText = function () {
    return this.text;
}

Node.prototype.getSelector = function () {
    return this.selector;
}

Node.prototype.getLinkEstablished = function () {
    return this.linkEstablished;
}

Node.prototype.draw = function () {
    
    //gain access to constructor variables
    var selector = this.selector;
    var nArr = this.nArr;
    var sArr = this.sArr;
    var paper = this.paper;
    var nodeSelected = this.nodeSelected;

    //TODO : use the Node to access the private variables instead of the crap above this 
    var Node = this;  //<---- I think this is the right way to do it

    //create the node with the properties in nArr
    var node = this.paper.rect(this.nArr.xl, this.nArr.yl, this.nArr.width, this.nArr.height);

    //set the attributes
    node.attr({
        fill: this.nArr.fill
    });

    //draw the initial selector
    selector.draw();

    //draw the initial text
    Node.text = this.paper.text(this.nArr.xl + 10, this.nArr.yl + 15, this.nArr.id).attr({ "font-size": "20px" });


    //
    //
    //
    //
    //test id
    node.node.setAttribute('id', 'newClass');


    //
    //
    //
    //
    //




    // Register Events
    // node.node accesses the dom element

    //create a selection handler around the node when I hover over it
    node.mouseover(function (e) {

        //change the pointer to a css pointer
        node.attr({
            cursor: "pointer"
        });

        //show the handle when hovering over the node
        if (selector != null)
            selector.show();

    }).mouseout(function (e) {
        if(!Node.nodeSelected)
            //clear the selector
            selector.clear();
    });

    //handle node dragging
    var start = function () {
        //ox is original X position
        this.ox = this.attr("x");
        //oy is original y position
        this.oy = this.attr("y");
        //animate on start
        this.animate({ opacity: .25 }, 500, ">");
    },
    //dx, dy is mouse position within the canvas
    move = function (dx, dy) {
        //calculate displacement and set new position for node
        this.attr({ x: this.ox + dx, y: this.oy + dy });
        //calculate displacement and set new position for selector
        selector.getSelector().attr({ x: this.ox + dx, y: this.oy + dy });
        sArr.xl = this.ox + dx;
        sArr.yl = this.oy + dy;
        //calculate displaced and set new position for text
        Node.text.attr({ x: this.ox + dx + 10, y: this.oy + dy + 15 });

        selector.redraw();
    },
    up = function () {
        //animate on mouseup
        this.animate({ opacity: 1.0 }, 500, ">");
    };

    //add the drag event linking the move, start, and up functions
    node.drag(move, start, up);

    //selection handler shows when clicked
    node.click(function () {
        //debug - output the id of the node being clicked
        console.log("You clicked on me, node Number: " + nArr.id);
        //display the selector
        selector.show();

        //node is selected
        Node.nodeSelected = true;
    });

    //deselect all when canvas is clicked
    background.click(function () {
        //clear the selector
        selector.clear();

        //node is not selected
        Node.nodeSelected = false;










        //try JSPLUMB
        jsPlumb.importDefaults({
            // default drag options
            DragOptions: { cursor: 'pointer', zIndex: 2000 },
            // default to blue at one end and green at the other
            EndpointStyles: [{ fillStyle: '#225588' }, { fillStyle: '#558822' }],
            // blue endpoints 7 px; green endpoints 11.
            Endpoints: [["Dot", { radius: 7 }], ["Dot", { radius: 11 }]],
            // the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
            // case it returns the 'labelText' member that we set on each connection in the 'init' method below.
            ConnectionOverlays: [
                ["Arrow", { location: 0.9 }]
            ],
            ConnectorZIndex: 5
        });

        // this is the paint style for the connecting lines..
        var connectorPaintStyle = {
            lineWidth: 5,
            strokeStyle: "#deea18",
            joinstyle: "round",
            outlineColor: "white",
            outlineWidth: 7
        },
        // .. and this is the hover style. 
        connectorHoverStyle = {
            lineWidth: 7,
            strokeStyle: "#2e2aF8"
        },
        // the definition of source endpoints (the small blue ones)
        sourceEndpoint = {
            endpoint: "Dot",
            paintStyle: { fillStyle: "#225588", radius: 7 },
            isSource: true,
            connector: ["Flowchart", { stub: [40, 60], gap: 10 }],
            connectorStyle: connectorPaintStyle,
            hoverPaintStyle: connectorHoverStyle,
            connectorHoverStyle: connectorHoverStyle,
            dragOptions: {},
            overlays: [
                ["Label", {
                    location: [0.5, 1.5],
                    label: "Drag"
                }]
            ]
        },
        // a source endpoint that sits at BottomCenter
    //	bottomSource = jsPlumb.extend( { anchor:"BottomCenter" }, sourceEndpoint),
        // the definition of target endpoints (will appear when the user drags a connection) 
        targetEndpoint = {
            endpoint: "Dot",
            paintStyle: { fillStyle: "#558822", radius: 11 },
            hoverPaintStyle: connectorHoverStyle,
            maxConnections: -1,
            isTarget: true
        };

        /*
        var allSourceEndpoints = [], allTargetEndpoints = [];
        _addEndpoints = function (toId, sourceAnchors, targetAnchors) {
            for (var i = 0; i < sourceAnchors.length; i++) {
                var sourceUUID = toId + sourceAnchors[i];
                allSourceEndpoints.push();
            }
            for (var j = 0; j < targetAnchors.length; j++) {
                var targetUUID = toId + targetAnchors[j];
                allTargetEndpoints.push(jsPlumb.addEndpoint(toId, targetEndpoint, { anchor: targetAnchors[j], uuid: targetUUID }));
            }
        };
        */
        //_addEndpoints("newClass", ["TopCenter", "BottomCenter"], ["LeftMiddle", "RightMiddle"]);

        jsPlumb.addEndpoint("newClass", targetEndpoint, { anchor: "TopCenter", uuid: "newClassTopCenter" })

        // listen for new connections; initialise them the same way we initialise the connections at startup.

        //* connect a few up
        //jsPlumb.connect({ uuids: ["window2BottomCenter", "window3TopCenter"] });
    });

    //set node reference
    this.node = node;
}