﻿function Selector(paper, sArr) {
    this.sArr = sArr;
    this.paper = paper;
    this.selector = null;
}

Selector.prototype.setSelector = function (selector) {
    this.selector = selector;
}

Selector.prototype.getSelector = function () {
    return this.selector;
}

Selector.prototype.getsArr = function () {
    return sArr;
}

Selector.prototype.draw = function () {

    selector = this.paper.rect(this.sArr.xl, this.sArr.yl, this.sArr.width, this.sArr.height);

    selector.attr({
        "stroke-width": 3,
        "stroke": "#2F2F4F"
    });

    selector.hide();

    this.setSelector(selector);
}

Selector.prototype.redraw = function () {
    this.getSelector().attr({
        "stroke-width": 3,
        "stroke": "#2F2F4F",
        x: this.sArr.xl,
        y: this.sArr.yl
    });
}

Selector.prototype.clear = function () {
    this.selector.hide();
}

Selector.prototype.show = function () {
    this.selector.show();
}